# Setup Local

Setup alternativo até conseguir achar uma solução para trabalhar com o /academy e /home no docker

1- Faça o git clone deste projeto

```
git clone git@bitbucket.org:igormmda/ab-inbev-docker.git
```
Execute o comando de setup automático
```
make install
```

Acesse o arquivo 'academy/modules/custom/consumeros/consumeros.module' descomente a linha 107 e deixe ela da seguinte forma
```
$site_root = '/';
```

> Não envie essa alteração, pois ela poderá impactar no funcionamento dos ambientes do cliente e AWS

Limpe os caches do Drupal e tente acessar o site 