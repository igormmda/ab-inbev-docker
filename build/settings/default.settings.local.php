<?php

$config_directories['sync'] = 'config/sync';

// Database
$databases['default']['default'] = array (
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'drupal',
  'host' => 'mariadb',
  'prefix' => '',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

// Settings overrride
$settings['container_yamls'] = [DRUPAL_ROOT . '/sites/default/services.local.yml'];
$settings['hash_salt'] = '04ec9ecf6d65c5cf8751c65bb80dcd5f17edf16aad5fcb97aa3019c162d5b629';
$settings['file_private_path'] = 'sites/default/private-files';
$settings['file_public_path'] = 'sites/default/files';
// $settings['trusted_host_patterns'] = [
//   '^consumeros\.com$',
//   '^consumeros$',
// ];

// custom settings
//$settings['site_root'] = '/';
$settings['consumeros_login_url'] = 'https://ghq-consumerplatforms-front-sso.azurewebsites.net/home/SignIn';
$settings['consumeros_client_id'] = '2563d247-4001-4428-af9a-811ff842e25f';
$settings['academy_title_max_length'] = 37;
$settings['consumeros_skip_verification'] = false;
$settings['cos_api_origin'] = 'http://dev-consumeros.ab-inbev.com';

//$settings['consumeros_endpoint_menu'] =  'https://uam-api.azurewebsites.net/api/Menu/find-by-app/2';
//$settings['consumeros_endpoint_token'] = 'https://uam-api.azurewebsites.net/api/Token';
//$settings['consumeros_endpoint_email'] = 'https://uam-api.azurewebsites.net/api/User/Email';

// Overrride
$config['system.file']['path']['private'] = 'sites/default/private-files';
//$config['system.mail']['interface']['webform'] = 'devel_mail_log';
$config['devel.settings']['debug_mail_directory'] = 'temporary://mails';
$config['devel.settings']['debug_mail_file_format'] = 'devel-mail-%to-%subject-%datetime.mail.txt';
$config['system.logging']['error_level'] = 'verbose';

$config['az_blob_fs.settings']['az_blob_account_name'] = 'devcosstorage';
$config['az_blob_fs.settings']['az_blob_account_key'] = 'y5WZMFMcrarMwFw+cpsV7b4QiaageIv0F72+BoQdYXW3gYUfOLcNB/Lc4IcYM+teyl9d7Yu9WMz2YjBTOFJCNQ==';
$config['az_blob_fs.settings']['az_blob_container_name'] = 'academy';
$config['sendgrid_integration.settings']['apikey'] = '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';

$optimize = false;
//$optimize = TRUE;
$preprocess = $optimize;
$minify = $optimize;
$strip_comments = $optimize;
$cache = $optimize;

$config['system.performance']['css']['preprocess'] = $preprocess;
$config['system.performance']['js']['preprocess'] = $preprocess;
$config['minifyhtml.config']['strip_comments'] = $strip_comments;
$config['minifyhtml.config']['minify'] = $minify;

if (!$optimize) {
  $cache_bins = [
    'bootstrap',
    'config',
    'data',
    'default',
    'discovery',
    'discovery_migration',
    'dynamic_page_cache',
    'entity',
    'jsonapi_resource_types',
    'menu',
    'migrate',
    'page',
    'render',
    'rest',
    'static',
    'toolbar',
    'youtube'
  ];

  foreach ($cache_bins as $bin) {
    $settings['cache']['bins'][$bin] = 'cache.backend.null';
  }
}

// PHP settings overrride
error_reporting(E_ALL);
ini_set('memory_limit', '254M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('apc.enabled', FALSE);