# ----------------
# Make help script
# ----------------

# Usage:
# Add help text after target name starting with '\#\#'
# A category can be added with @category. Team defaults:
# 	dev-environment
# 	docker
# 	drush

# Output colors
GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)

#
# Dev Environment settings
#
-include .env



TAG ?= ${TAG}
NAME = ciencia-na-escola
REPO = mmda/${NAME}

REPOREMOTE=962060631254.dkr.ecr.sa-east-1.amazonaws.com/${NAME}
REGISTRY=mmda\/${NAME}:$(TAG) 
NAMESPACE ?=
DEPLOY=rnp-deploy
PVC=vol-drupal-$(NAMESPACE) 

DRUPAL_WEB_ROOT ?= $(NGINX_SERVER_ROOT)
DRUPAL_ROOT ?= $(NGINX_SERVER_ROOT)

#
# Dev Operations
#

default: up

up: ##@docker Start containers and display status.
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose up -d --remove-orphans
	docker-compose ps
	@echo "Site at http://$(PROJECT_BASE_URL):8000/"


install: ##@dev-environment Configure development environment.
	cp .env.example .env
	make down
	make up
	make clone-repo
	make composer-install
	cp build/settings/default.settings.local.php martech-school/sites/default/settings.local.php
	cp build/settings/default.settings.php martech-school/sites/default/settings.php
	cp build/settings/default.services.local.yml martech-school/sites/default/services.local.yml
	make import-db
	make prep-site
	# 	@echo "Pulling database for $(PROJECT_NAME)..."
    # 	make pull-db
    # 	make pull-files
    # 	@echo "Development environment for $(PROJECT_NAME) is ready."
	make uli

site-install:
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_WEB_ROOT) si --site-name=$(PROJECT_NAME) --account-name=admin --account-pass=1 -y

import-db: ##@dev-environment Import locally cached copy of `database.sql` to project dir.
	@echo "Dropping old database for $(PROJECT_NAME)..."
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_ROOT) sql-drop -y
	@echo "Importing database for $(PROJECT_NAME)..."
	pv build/db/database.sql | docker exec -i $(PROJECT_NAME)_mariadb mysql -u$(DB_USER) -p$(DB_PASSWORD) $(DB_NAME)

prep-site: ##@docker Prepare website.
	make updb
	make cim
	make cr

clone-repo:
	if [ -d "martech-school" ]; then echo "Directory alredy exists"; \
    else git clone --branch academy git@bitbucket.org:mmda/ab-inbev-academy.git martech-school; \
    fi

start: ##@docker Start containers and display status.
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose up --remove-orphans

down: stop

stop: ##@docker Stop and remove containers.
	@echo "Stopping containers for $(PROJECT_NAME)..."
	@docker-compose stop

prune: ##@docker Remove containers for project.
	@echo "Removing containers for $(PROJECT_NAME)..."
	@docker-compose down -v

ps: ##@docker List containers.
	@docker ps --filter name='$(PROJECT_NAME)*'

shell: ##@docker Shell into the container. Specify container name.
	docker exec -ti -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") sh

shell-nginx: ##@docker Shell into the container. Specify container name.
	docker exec -ti -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(shell docker ps --filter name='$(PROJECT_NAME)_nginx' --format "{{ .ID }}") sh


drush: ##@docker Run arbitrary drush commands.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_WEB_ROOT) $(filter-out $@,$(MAKECMDGOALS))

logs: ##@docker Display log.
	docker-compose logs

composer-update: ##@dev-environment Run composer update.
	docker-compose exec -T php composer update -n --prefer-dist -vvv -d $(DRUPAL_ROOT)

composer-install: ##@dev-environment Run composer install
	docker-compose exec -T php composer install -n --prefer-dist -vvv -d $(DRUPAL_ROOT)

create-solr-core:
	@docker exec -ti $(PROJECT_NAME)_solr make core=$(PROJECT_NAME)_solr -f /usr/local/bin/actions.mk

cr: ##Clear caches.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_WEB_ROOT) cr

cim: ##drush Drush import configuration.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_ROOT) config-import

cex: ##drush Drush import configuration.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_ROOT) config-export

updb: ##drush run database updates.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_ROOT) updb -v -y

uli: ##drush Generate login link.
	docker exec $(shell docker ps --filter name='$(PROJECT_NAME)_php' --format "{{ .ID }}") drush -r $(DRUPAL_WEB_ROOT) --uri='$(PROJECT_BASE_URL):8000' uli
#pull-files: ##@dev-environment Download files from platform.sh.
#	@echo "Downloading files from (PROJECT_NAME)..."
#	platform mount:download --mount="docroot/sites/default/files" --target=docroot/sites/default/files -y -v

